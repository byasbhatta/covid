package com.mh.covid.util;

import java.time.LocalDate;

/**
 * @author Sunil Babu Shrestha on 5/20/2020
 */
public class DateUtil {

    public static int getAgeFromBornYear(int bornYear){
        return LocalDate.now().getYear()-bornYear;
    }
}
