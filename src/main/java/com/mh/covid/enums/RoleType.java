package com.mh.covid.enums;

public enum RoleType {
    ADMIN, OPERATOR;

    public static RoleType getEnumFromString(String roleStr) {
        RoleType[] roles = RoleType.values();
        for (RoleType role : roles) {
            if (role.toString().equals(roleStr)) {
                return role;
            }
        }
        throw new RuntimeException("Enum value not exits for given String " + roleStr);
    }

}
