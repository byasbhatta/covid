//package com.mh.covid.servlet;
//
//import com.mh.covid.dao.EmployeeDoa;
//import com.mh.covid.dto.Employee;
//import com.mh.covid.enums.RoleTypes;
//
//import javax.servlet.ServletException;
//import javax.servlet.http.HttpServlet;
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//import java.io.IOException;
//
//public class EmployeeServlet extends HttpServlet {
//    @Override
//    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
//        System.out.println("Get Method Invoked");
//    }
//
//    @Override
//    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
//        System.out.println("Post Method Invoked");
//        Employee employee=new Employee();
//        employee.setName(req.getParameter("name"));
//        employee.setEmail(req.getParameter("email"));
//        employee.setPassword(req.getParameter("password"));
//        employee.setAddress(req.getParameter("address"));
//        String role=req.getParameter("role");
//        employee.setRoleTypes(RoleTypes.getEnumFromString(role));
//
//        EmployeeDoa employeeDoa=new EmployeeDoa();
//        boolean result=employeeDoa.SaveEmployee(employee);
//        if(result){
//           resp.sendRedirect("employee.jsp?msg=1");
//        }
//        else {
//            resp.sendRedirect("error.jsp");
//        }
//    }
//}
