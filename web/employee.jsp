<%@ page import="com.mh.covid.enums.RoleType" %>
<%@ page import="com.mh.covid.dao.EmployeeDao" %>
<%@ page import="java.util.List" %>
<%@ page import="com.mh.covid.dto.Employee" %><%--
  Created by IntelliJ IDEA.
  User: AshokLaxmi
  Date: 5/25/20
  Time: 5:15 PM
  To change this template use File | Settings | File Templates.
--%>
<%--
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
    <link rel="stylesheet" href="css/bootstrap.css">
</head>
<body>
<div class="container-fluid">
    <div class="bg-primary">
        <%
            String msg = request.getParameter("msg");
            if(null != msg && msg.equals("1")) {
                out.print("Data Successfully Updated");
            }
        %>

    </div>
    <form action="request.jsp" method="post">
        <div class="form-row">
            <div class="form-group col-md-6">
                <label for="inputEmail4">Email</label>
                <input type="email" class="form-control" id="inputEmail4" name="email">
            </div>
            <div class="form-group col-md-6">
                <label for="inputPassword4">Password</label>
                <input type="password" class="form-control" id="inputPassword4" name="Password">
            </div>
        </div>
        <div class="form-group">
            <label for="inputAddress">Address</label>
            <input type="text" class="form-control" id="inputAddress" placeholder="1234 Main St" name="address">
        </div>
        <div class="form-row">
            <div class="form-group col-md-6">
                <label for="name">Name</label>
                <input type="text" class="form-control" id="name" name="name">
            </div>
            <div class="form-group col-md-4">
                <label for="inputState">Role</label>
                <select id="inputState" class="form-control" name="role">
                    <option selected>Choose...</option>
                    <%
                        RoleType[] roles= RoleType.values();
                        for (RoleType role: roles){
                %>
                    <option> <%=role%>
                    </option>
                    <%}%>
                </select>
            </div>
            </div>
            <button type="submit" class="btn btn-primary">Save</button>
    </form>
</div>
</body>
</html>
--%>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<html>
<head>
<title> Title</title>
    <link rel="stylesheet" href="css/bootstrap.css">
</head>
<body>
<div class ="container-fluid">
    <form action="/employee" method="post...">
        <table class="table table-sm">
<thead>
<tr>

            <th scope="col">#</th>
            <th scope="col">Name</th>
            <th scope="col">Email</th>
            <th scope="col">Address</th>
            <th scope="col">Password</th>
            <th scope="col">role</th>
        </tr>
        </thead>
        <tbody>
        <%
            EmployeeDao dao= new EmployeeDao();
            List<Employee> employeeList = dao.getEmployees();
            for (Employee emp : employeeList) {
        %>
        <tr>
            <th scope="row">1</th>
            <td><%=emp.getName()%></td>
            <td><%=emp.getEmail()%></td>
            <td><%=emp.getAddress()%></td>
            <td><%=emp.getPassword()%></td>
            <td><%=emp.getRoleType()%></td>
            <td><a class="btn btn-sm btn-outline-danger" href="">Delete</a></td>
        </tr>
        <%}%>

        </tbody>
    </table>
    </form>
</div>
</body>
</html>