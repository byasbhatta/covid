<%@ page import="java.time.LocalDate" %>
<%@ page import="javax.print.attribute.standard.PagesPerMinute" %>
<%@ page import="com.mh.covid.util.DateUtil" %>
<%@ page import="com.mh.covid.dto.Employee" %>
<%@ page import="com.mh.covid.enums.RoleType" %>
<%@ page import="com.mh.covid.dao.EmployeeDao" %><%--
  Created by IntelliJ IDEA.
  User: AshokLaxmi
  Date: 5/25/20
  Time: 7:46 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
</head>
<body>
<%
    Employee employee = new Employee();
    employee.setName(request.getParameter("name"));
    employee.setEmail(request.getParameter("email"));
    employee.setPassword(request.getParameter("password"));
    employee.setAddress(request.getParameter("address"));
    String role = request.getParameter("role");
    employee.setRoleType(RoleType.getEnumFromString(role));

    EmployeeDao employeeDao = new EmployeeDao();
    boolean result = employeeDao.saveEmployee(employee);

    if (result) {
        out.println("Successfully save record on db");
    } else
    {
        out.println("Failed to save record on db");
    }
%>
</body>
</html>
