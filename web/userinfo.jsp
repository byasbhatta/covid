<%@ page import="java.util.List" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.util.Enumeration" %>
<%@ page import="java.sql.ClientInfoStatus" %>
<%@ page import="java.time.LocalDate" %><%--
  Created by IntelliJ IDEA.
  User: AshokLaxmi
  Date: 5/25/20
  Time: 7:59 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">

    <!-- jQuery library -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

    <!-- Latest compiled JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
</head>
<body>
<%
    int i=0;
    int index=0;
    List<String> userName = new ArrayList<>();
    List<Integer> bornDates = new ArrayList<>();
    while (request.getParameter("userName"+1)!=null)
    {
        userName.add(request.getParameter("usrName"+i));
        bornDates.add(Integer.parseInt(request.getParameter("bornYear"+i)));
    if (Integer.parseInt(request.getParameter("bornYear"+i))>Integer.parseInt(request.getParameter("bornYear"+index)))
    {
        index=i;
    }
    i++;
    }
%>
<div class="container">
    <table class="table table-striped">
        <thead>
        <tr>
            <th>User Name</th>
            <th>Age</th>
        </tr>
        </thead>
        <tbody>
        <%
            for (int j=0;j<userName.size();j++)
            {
        %>
        <tr>
            <td><%=userName.get(j)%></td>
            <td><%=LocalDate.now().getYear()-bornDates.get(j)%></td>
        </tr>
        <%
            }
        %>
        <tr>
            <td colspan="2">User :<%userName.get(index)%> is youngest User and his/her date of Birth is :<%bornDates.get(index)%></td>
        </tr>
        </tbody>
    </table>
</div>
</body>
</html>
